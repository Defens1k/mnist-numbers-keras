import pandas as pd
def get_byte(f):
    return ord(f.read(1))

def get_32_bit(f):
    value = 0
    for i in range(int(32 / 8)):
        value = value * (2 ** 8) +  ord(f.read(1))
    return value


def magic_number(f):
    value = get_32_bit(f)
    print("Magic number is ",value)
    return value

def read_y_head(f):
    values = list()
    values.append(magic_number(f))
    values.append(get_32_bit(f))
    print(values)
    return values

def read_pixels(f, count_rows, count_columns):
    pixels = list()
    for row in range(count_rows):
        for col in range(count_columns):
            pixels.append(ord(f.read(1)))
    return pixels


def read_y(f, count):
    data = pd.DataFrame()
    for i in range(10):
        data[str(i)] = i
    for i in range(count):
        print(i)
        row = list()
        y = get_byte(f)
        for j in range(y):
            row.append(0)
        row.append(1)
        for j in range(9 - y):
            row.append(0)
        data.loc[i] = row
        print(row)
        print(y)
        print()
    return data

def read_test_y():
    test_y_file = open('../input/test_y_byte', 'rb')
    magic_number, count = read_y_head(test_y_file)
    data = read_y(test_y_file, count)
    data.to_csv('input/test_y.csv')
    print(data)

def read_input():
    test_y = read_test_y()

read_input()
