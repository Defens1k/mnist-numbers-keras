import pandas as pd
def get_byte(f):
    return ord(f.read(1))

def get_32_bit(f):
    value = 0
    for i in range(int(32 / 8)):
        value = value * (2 ** 8) +  ord(f.read(1))
    return value


def magic_number(f):
    value = get_32_bit(f)
    print("Magic number is ",value)
    return value

def read_x_head(f):
    values = list()
    values.append(magic_number(f))
    for i in ("count_x", "count_rows", "count_columns"):
        values.append(get_32_bit(f))
    return values

def read_pixels(f, count_rows, count_columns):
    pixels = list()
    for row in range(count_rows):
        for col in range(count_columns):
            pixels.append(ord(f.read(1)))
    return pixels


def read_x(f, count_x, count_rows, count_columns):
    data = pd.DataFrame()
    for row in range(count_rows):
        for col in range(count_columns):
            data[str(row) + '-' + str(col)] = row
    for i in range(count_x):
        print(i)
        data.loc[i] = read_pixels(f, count_rows, count_columns)
    return data

def read_train_x():
    train_x_file = open('../input/train_x_byte', 'rb')
    magic_number, count_x, count_rows, count_columns = read_x_head(train_x_file)
    data = read_x(train_x_file, count_x, count_rows, count_columns)
    data.to_csv('input/train_x.csv')
    print(data)

def read_input():
    train_x = read_train_x()

read_input()
