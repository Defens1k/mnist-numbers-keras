import pathlib

import os
from multiprocessing import Process
import multiprocessing 
from time import sleep
from threading import Thread
import PIL
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from keras.models import load_model
from math import log2

EPOCHS = 500

test_x = pd.read_csv('../input/test_x.csv')
test_y = pd.read_csv('../input/test_y.csv')

A_test_x = test_x.copy()

test_y.drop(columns = ['A'], inplace = True)
test_x.drop(columns = ['A'], inplace = True)

vlist = test_y.values.tolist()



#model = load_model('../models/first')


img = PIL.Image.new(mode = 'L', size = (2800, 2800))
def img_proc(n, vlist, A_test_x, test_x, return_dict, percent_dict):
    local_img = PIL.Image.new(mode = 'L', size = (2800, 28))
    print("model")
    model = load_model('../models/first')
    print("load")
    true = 0
    false = 0
    for pos in range(100):
        print(n, " ", pos)
        row = A_test_x[A_test_x['A'] == n * 100 + pos]
        row.drop(columns = ['A'], inplace = True)
        res = model.predict(row)
        res = res[0]
        max = -1
        num = -1
        for i in range(10):
            if res[i] > max:
                max = res[i]
                num = i
        if vlist[n * 100 + pos][num] == 0:
            false += 1
            for i in range(28):
                for j in range(28):
                    a = list()
                    a.append(test_x.loc[n * 100 + pos][str(i) + '-' + str(j)])
                    local_img.putpixel(((28 * pos + j),(i)), tuple(a))
        else:
            true += 1
            for i in range(28):
                for j in range(28):
                    a = list()
                    a.append(255 - test_x.loc[n * 100 + pos][str(i) + '-' + str(j)])
                    local_img.putpixel(((28 * pos + j),(i)), tuple(a))
    return_dict[n] = local_img
    percent_dict["true"] += true
    percent_dict["false"] += false

list_proc = list()
manager = multiprocessing.Manager()
return_dict = manager.dict()
percent_dict = manager.dict()
percent_dict["false"] = 0
percent_dict["true"] = 0
for k in range(10):
    for n in range(10):
        p = Process(target = img_proc, args = (k * 10 + n, vlist.copy(), A_test_x.copy(), test_x.copy(), return_dict, percent_dict))
        p.start()
        list_proc.append(p)


    for i in list_proc:
        i.join()

for key, val in return_dict.items():
    img.paste(val, (0, 28 * key))
img.show()
img.save('./image.bmp')
print("NS predict fail " + str(percent_dict["false"]) + " of " + str(percent_dict["false"] + percent_dict["true"]))
exit()

