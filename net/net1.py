import pathlib

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from keras.models import load_model
from math import log2

EPOCHS = 400

test_x = pd.read_csv('../input/test_x.csv')
test_y = pd.read_csv('../input/test_y.csv')
train_x = pd.read_csv('../input/train_x.csv')
train_y = pd.read_csv('../input/train_y.csv')

train_y.drop(columns = ['A'], inplace = True)
train_x.drop(columns = ['A'], inplace = True)
test_y.drop(columns = ['A'], inplace = True)
test_x.drop(columns = ['A'], inplace = True)


print(test_x)
print(test_y)
print(train_x)
print(train_y)



def build_model():
  model = keras.Sequential([
    layers.Dense(400, activation='sigmoid', input_shape=[len(train_x.keys())]),
    layers.Dense(10, activation = 'softmax')
  ])

  optimizer = tf.keras.optimizers.RMSprop(0.002)

  model.compile(loss='mae',
                optimizer=optimizer,
                metrics=['mape', 'mae', 'mse'])
  return model

model = build_model()


class PrintDot(keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs):
    print('Epoch is ',epoch)



history = model.fit(
  train_x, train_y,
  epochs=EPOCHS, validation_split = 0.2, verbose=0,
  callbacks=[PrintDot()])


hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch
print (hist)

def plot_history(history):
  hist = pd.DataFrame(history.history)
  hist['epoch'] = history.epoch

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Error [MPG]')
  plt.plot(hist['epoch'], hist['mae'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mae'],
           label = 'Val Error')
  plt.ylim([0,0.1])
  plt.legend()

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Percent Error [MPG]')
  plt.plot(hist['epoch'], hist['mape'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mape'],
           label = 'Val Error')
  plt.ylim([0,100])
  plt.legend()


  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Square Error [$MPG^2$]')
  plt.plot(hist['epoch'], hist['mse'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mse'],
           label = 'Val Error')
  plt.ylim([0,0.1])
  plt.legend()
  plt.show()


plot_history(history)


#print(model.predict(test_x.loc[1]))



res = model.evaluate(test_x, test_y, verbose=2)

print(res)
#print("Testing set Mean Abs Error: {:5.2f} MPG".format(mape))


model.save('../models/first')

exit()
df = pd.read_csv('../input/10_ceras.csv')

df = df[df['year'] != 2019]

df.drop(columns = ['quantity_name'], inplace = True)
dff = df.copy()

russian = df[df['country_or_area'] == 'Russian Federation']
all_country = df[df['country_or_area'] != 'Russian Federation']

russian_export = russian[russian['flow'] == 'Export']
all_country_import = all_country[all_country['flow'] == 'Import']

graph_list = {}
def graph_list_commodity():
    for commodity in russian_export['commodity'].unique():
        
        value = russian_export[russian_export['commodity'] == commodity]
        value = value.groupby(['year'],as_index=False)['weight_kg','quantity'].agg('sum')

        import_graph = all_country_import[all_country_import['commodity'] == commodity]
        import_graph = import_graph.groupby(['year'],as_index=False)['weight_kg','quantity'].agg('sum')



        value['quantity'] = import_graph['weight_kg']

        value.rename(columns={'weight_kg': 'russian_export', 'quantity': 'world_import'}, inplace=True)

        graph_list[commodity] = value

graph_list_commodity()

def log_graphs():
    for i in graph_list.keys():
        new_col = list()
        for j in graph_list[i]['russian_export']:
            new_col.append(log2(j))
        graph_list[i]['russian_export'] = new_col
        new_col = list()
        for j in graph_list[i]['world_import']:
            new_col.append(log2(j))
        graph_list[i]['world_import'] = new_col

first_year = 1996
window_len = 7
count_years = (2018 - first_year + 1)
def splited():
    my_index = 0    


    new_pd = pd.DataFrame()
    for i in range(window_len):
        new_pd['r_e-' + str(i)] = i  
    for i in range(window_len):
        new_pd['w_i-' + str(i)] = i  

    errors = dict()
    for i in graph_list.keys():
        for left in range(count_years - window_len + 1):
            try:
                row = list()
                row_exp = list()
                row_imp = list()
                for pos in range(window_len):
                    row_exp.append(float(graph_list[i][graph_list[i]['year'] == first_year + left + pos]['russian_export']))
                for pos in range(window_len):
                    row_imp.append(float(graph_list[i][graph_list[i]['year'] == first_year + left + pos]['world_import']))
                def norm(input_row):
                    normed_row = list()
                    last = input_row.pop()
                    maximum = max(input_row)
                    minimum = min(input_row)
                    for col in input_row:
                        normed_row.append((col - minimum) / (maximum - minimum))
                    normed_row.append((last - minimum) / (maximum - minimum))
                    return normed_row
                row = norm(row_exp) + norm(row_imp)
                new_pd.loc[my_index] = row
                my_index += 1
            except:
                if i in errors.keys():
                    if first_year + left + pos not in errors[i]:

                        errors[i].append(first_year + left + pos)
                else:
                    errors[i] = list()
                    errors[i].append(first_year + left + pos)
    return new_pd



log_graphs()
normed_data = splited()

normed_train = normed_data.sample(frac=0.99,random_state=0)
normed_test = normed_data.drop(normed_train.index)

normed_train_x = normed_train.copy()
normed_train_x.drop(columns = ['r_e-' + str(window_len - 1)], inplace = True)
normed_train_x.drop(columns = ['w_i-' + str(window_len - 1)], inplace = True)

normed_train_y = normed_train.copy()
normed_train_y.drop(columns = ['w_i-' + str(window_len -1)], inplace = True)
for i in range(window_len - 1):
    normed_train_y.drop(columns = ['w_i-' + str(i)], inplace = True)
    normed_train_y.drop(columns = ['r_e-' + str(i)], inplace = True)

normed_test_x = normed_test.copy()
normed_test_x.drop(columns = ['r_e-' + str(window_len - 1)], inplace = True)
normed_test_x.drop(columns = ['w_i-' + str(window_len - 1)], inplace = True)

normed_test_y = normed_test.copy()
normed_test_y.drop(columns = ['w_i-' + str(window_len -1)], inplace = True)
for i in range(window_len - 1):
    normed_test_y.drop(columns = ['w_i-' + str(i)], inplace = True)
    normed_test_y.drop(columns = ['r_e-' + str(i)], inplace = True)





def build_model():
  model = keras.Sequential([
    layers.Dense(200, activation='sigmoid', input_shape=[len(normed_train_x.keys())]),
   # layers.Dense(100, activation='sigmoid'),
    layers.Dense(200, activation='sigmoid'),
    layers.Dense(1)
  ])

  optimizer = tf.keras.optimizers.RMSprop(0.0001)

  model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mape', 'mae', 'mse'])
  return model

model = build_model()

model.summary()


best_epoch_score = 99999999999999
best_epoch = 0
class PrintDot(keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs):
    global best_epoch_score
    global best_epoch
    if logs['mse'] < best_epoch_score:
        best_epoch = epoch
        best_epoch_score = logs['mse']
        model.save('models/best')
    if 0 == 0:
        model.save('models/' + str(epoch))
    if epoch % 100 == 0: print('')
    print('.', end='')

EPOCHS = 310

history = model.fit(
  normed_train_x, normed_train_y,
  epochs=EPOCHS, validation_split = 0.01, verbose=0,
  callbacks=[PrintDot()])

print("best epoch is ", best_epoch)

model = load_model('models/best')


hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch

def plot_history(history):
  hist = pd.DataFrame(history.history)
  hist['epoch'] = history.epoch

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Error [MPG]')
  plt.plot(hist['epoch'], hist['mae'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mae'],
           label = 'Val Error')
  plt.ylim([0,1])
  plt.legend()

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Percent Error [MPG]')
  plt.plot(hist['epoch'], hist['mape'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mape'],
           label = 'Val Error')
  plt.ylim([0,60])
  plt.legend()


  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Square Error [$MPG^2$]')
  plt.plot(hist['epoch'], hist['mse'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mse'],
           label = 'Val Error')
  plt.ylim([0,1])
  plt.legend()
  plt.show()


plot_history(history)


loss, mape, mae, mse = model.evaluate(normed_test_x, normed_test_y, verbose=2)

print("Testing set Mean Abs Error: {:5.2f} MPG".format(mape))



def pred(left, right, model):
    global dff
    df = dff.copy()
    df_back = df.copy()
    df = df[df['year'] < right]
    df = df[df['year'] >= left]
    russian = df[df['country_or_area'] == 'Russian Federation']
    all_country = df[df['country_or_area'] != 'Russian Federation']

    russian_export = russian[russian['flow'] == 'Export']
    all_country_import = all_country[all_country['flow'] == 'Import']

    graph_list = 0

    value = russian_export.copy()
    value = value.groupby(['year'],as_index=False)['weight_kg','quantity'].agg('sum')

    import_graph = all_country_import.copy()
    import_graph = import_graph.groupby(['year'],as_index=False)['weight_kg','quantity'].agg('sum')



    value['quantity'] = import_graph['weight_kg']

    value.rename(columns={'weight_kg': 'russian_export', 'quantity': 'world_import'}, inplace=True)

    new_col = list()
    for j in value['russian_export']:
        new_col.append(log2(j))
    value['russian_export'] = new_col
    new_col = list()
    for j in value['world_import']:
        new_col.append(log2(j))
    value['world_import'] = new_col   

    def norm(input_row):
        normed_row = list()
        maximum = max(input_row)
        minimum = min(input_row)
        for col in input_row:
            normed_row.append((col - minimum) / (maximum - minimum))
        return maximum, minimum, normed_row


    x_e = list()
    x_i = list()
    for i in value['year'].unique():
        x_e.append(float(value[value['year'] == i]['russian_export']))
    for i in value['year'].unique():
        x_i.append(float(value[value['year'] == i]['world_import']))
    maximum, minimum, x_i = norm(x_i)
    maximum, minimum, x_e = norm(x_e)
    x = x_e + x_i
    test_y = pd.DataFrame()
    for i in range(len(x)):
        test_y[str(i)] = i
    test_y.loc[0] = x
    y = model.predict(test_y)
    y = y * (maximum - minimum) + minimum
    y = 2 ** y
    df = df_back
    return y

def pred_graph(M, str_epoch):
    global dff
    df = dff.copy()
    russian = df[df['country_or_area'] == 'Russian Federation']
    russian_export = russian[russian['flow'] == 'Export']
    value = russian_export.copy()
    value = value.groupby(['year'],as_index=False)['weight_kg','quantity'].agg('sum')
    data = value




    n = list()
    for i in range(window_len - 2):
        n.append(None)
    for i in range(first_year, first_year + count_years - window_len  + 2):
        a = pred(i, i + window_len - 1, M)
        n.append(float(a))

    plt.xlabel('Year')
    plt.ylabel('Weight, kg')
    ye = list()
    for i in data['year']:
        ye.append(i)
    ye.append(2019)
    n.append(5.6 * 10 ** 10)
    ye.append(2020)
    n.append(5.753123678 * 10 ** 10)
    ye.append(2021)
    n.append(4.66326473267 * 10 ** 10)

   
    plt.plot(ye, n, label = "Epoch is " + str_epoch, linewidth = 4)
    plt.legend()
    return data


plt.figure()
plt.minorticks_on()
plt.grid(which='minor', 
    color = 'k', 
    linestyle = ':')
plt.grid(which='major',
    color = 'k', 
    linewidth = 1)

data = pred_graph(model, "best")
plt.plot(data['year'], data['weight_kg'], label = "Original", linewidth = 4)
for i in range(EPOCHS):
    if i % 30 == 0:
        m = load_model('models/' + str(i))
        pred_graph(m, str(i))

plt.show()

ye = input()
if ye == "best":
    ye = best_epoch
    print("best epoch is", best_epoch)
else :
    ye = int(ye)

plt.figure()
plt.minorticks_on()
plt.grid(which='minor', 
    color = 'k', 
    linestyle = ':')
plt.grid(which='major',
    color = 'k', 
    linewidth = 1)


data = pred_graph(model, "best")
plt.plot(data['year'], data['weight_kg'], label = "Original", linewidth = 4)
for i in range(EPOCHS):
    if i == ye:
        m = load_model('models/' + str(i))
        pred_graph(m, str(i))

plt.show()
exit()



